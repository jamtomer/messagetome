package MessageToMe.demo.User;

import MessageToMe.demo.Message.Message;
import MessageToMe.demo.Paper.Paper;
import MessageToMe.demo.Reaction.Reaction;
import MessageToMe.demo.Sticker.Sticker;
import com.sun.istack.NotNull;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Table(name="User")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class User {

    @Id
    @NotNull
    @GeneratedValue
    @Column(name = "userId", unique = true)
    private Long userId;

    // 여기 유니크키를 달아야 할까요?
    @Column(name = "userName", unique = true)
    private String userName;

    @NotNull
    @Column(name = "email", unique = true)
    private String email;

    @NotNull
    @Column(name = "password")
    private String password;

    @CreatedDate
    @Column(name = "createDate")
    private LocalDateTime createDate;

    @Column(name = "authNum")
    private Integer authNum; // 수정: int -> Integer

    //카카오 가입용
    @Column(name = "idToken")
    private String idToken;

    @OneToMany(mappedBy = "createUser", cascade = CascadeType.ALL)
    private List<Paper> paperList = new ArrayList<>();

    @OneToMany(mappedBy = "ownerUser", cascade = CascadeType.ALL)
    private List<Paper> paperList2 = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Message> messageList = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Reaction> reactionList = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Sticker> stickerList = new ArrayList<>();

}