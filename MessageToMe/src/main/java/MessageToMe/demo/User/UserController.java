package MessageToMe.demo.User;
import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Secure.SecureService;
import MessageToMe.demo.User.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

@Slf4j
@RestController
public class UserController {

    @Autowired
    private JavaMailSender mailSender;
    private static final String FROM_ADDRESS = "messagetometeam@gmail.com";

    private final UserService userService;
    private final JWTService jwtService;
    private final SecureService secureService;

    public UserController(UserService userService, SecureService secureService, JWTService jwtService){
        this.userService = userService;
        this.jwtService = jwtService;
        this.secureService = secureService;
    }

    /**
     * 카카오 로그인 키값 리턴
     * @param key
     * @return
     */
    @GetMapping("/kakaoKey")
    public Map<String,String> KAKAOLogin(@RequestParam String key){
        Map<String, String> resultMap = new HashMap();
        try{
            log.info("================================= kakao Login Key Return ");
            String code = secureService.selectSecureKey(key);
            resultMap.put("result",code);
        } catch (Exception e){
            log.error(e.getMessage());
            resultMap.put("result","error");
        }
        return resultMap;
    }

    /**
     * 회원가입 : 개인정보 저장
     * @param userDTO
     * @return
     */
    @PostMapping(path = "/join")
    public ResponseEntity join(@RequestBody UserDTO userDTO){
        try{
            log.info("================================= user join start");

            //이메일 양식
            String userEmail = userDTO.getEmail();
            if (!Pattern.matches("^[a-zA-Z0-9+-\\_.]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$", userEmail)) {
                return new ResponseEntity<>("email expression error", HttpStatus.BAD_REQUEST); //400
            }
            //이메일 길이
            if (userEmail.length() > 500) {
                return new ResponseEntity<>("email length error", HttpStatus.BAD_REQUEST); //400
            }
            //이메일 중복
            if(userService.selectEmail(userEmail).equals("exist")){
                return new ResponseEntity<>("email exist", HttpStatus.CONFLICT); //409 리소스 충돌
            }
            //닉네임 길이 : 2 이상 8 이하
            String userName = userDTO.getUserName();
            if (userName.length() < 1 || 8 < userName.length()) {
                return new ResponseEntity<>("userName length error", HttpStatus.BAD_REQUEST); //400
            }
            //닉네임 중복
            if(userService.selectUserName(userName).equals("exist")){
                return new ResponseEntity<>("userName exist", HttpStatus.CONFLICT); //409 리소스 충돌
            }
            //비밀번호 길이 : 8 이상 500 이하
            if (userDTO.getPassword().length() < 8 || 500 < userDTO.getPassword().length() ){
                return new ResponseEntity<>("password length error", HttpStatus.BAD_REQUEST); //400
            }
            String result = userService.save(userDTO);
            if(result.equals("ok")){
                return new ResponseEntity<>("success", HttpStatus.OK);
            }
        } catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity<>("SERVER_ERROR (1) ", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("SERVER_ERROR (2) ", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 회원가입 : 이메일 중복검사
     * @param email
     * @return
     */
    @GetMapping("/validateEmail")
    public ResponseEntity validateEmail(@RequestParam String email){
        try{
            log.info("================================= user validateEmail start");
            if(userService.selectEmail(email).equals("ok")){
                return new ResponseEntity<>("OK", HttpStatus.OK);
            } else if(userService.selectEmail(email).equals("exist")){
                return new ResponseEntity<>("ALREADY_EXIST_EMAIL", HttpStatus.CONFLICT); //409 리소스 충돌
            }
        } catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity<>("SERVER_ERROR (1)", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("SERVER_ERROR (2)", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * 회원가입 : 닉네임 중복검사
     * @param userName
     * @return
     */
    @GetMapping("/validateName")
    public ResponseEntity validateName(@RequestParam String userName){
        try{
            log.info("================================= user validateName start");
            if(userService.selectUserName(userName).equals("ALREADY_EXIST_USER_NAME")){
                return new ResponseEntity<>("ALREADY_EXIST_USER_NAME", HttpStatus.CONFLICT); //409 리소스 충돌
            }
            return new ResponseEntity<>("OK", HttpStatus.OK);
        } catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity<>("SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 회원가입 : 본인 이메일 확인용 코드 전송
     * @param email
     * @return
     */
    @GetMapping("/checkEmail")
    public ResponseEntity checkEmail(@RequestParam String email){
        try{
            log.info("================================= user checkEmail start");
            int leftLimit = 48; // numeral '0'
            int rightLimit = 122; // letter 'z'
            int targetStringLength = 10;
            Random random = new Random();
            String setCode = random.ints(leftLimit,rightLimit + 1)
                    .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                    .limit(targetStringLength)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();

            //이메일 전송 : 근데 구글 이메일 일일전송 제한 있대요...... 저렴한 해결법 찾아봐야함...
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(email);
            message.setFrom(FROM_ADDRESS);
            message.setSubject("[ Message to me TEAM ] 본인 확인용 코드입니다.");
            message.setText("확인 코드 : " + setCode);
            mailSender.send(message);
            Map<String,String> code = new HashMap();
            code.put("code", setCode);

            return new ResponseEntity<>(setCode, HttpStatus.CREATED);
        }catch (Exception e){
            log.error(e.getMessage());
            return new ResponseEntity<>("SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 이메일코드 체크
     * @param map
     * @return
     */
    @PostMapping("/checkCode")
    public ResponseEntity checkCode(@RequestBody Map<String, String> map){
        //보안을 위해서 테이블 하나 생성하거나, 1분 단위로 스레드를 돌리던가 해야할 것 같은데 일단 보류
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    /**
     * 로그아웃
     * @param userDTO
     * @return
     */
    @PostMapping("/logout")
    public ResponseEntity logout(@RequestBody UserDTO userDTO){
        //redis 쓰라는 말이 있어서 보류함
        return new ResponseEntity<>("", HttpStatus.OK);
    }

    /**
     * 로그인
     * @param userDTO
     * @return
     */
    @PostMapping(path = "/login")
    public ResponseEntity login(@RequestBody UserDTO userDTO){
        try{
            log.info("================================= user login start");
            if (userDTO.getIdToken() != null) { //카카오로그인
                userDTO = userService.kakaoLogin(userDTO);
            } else {
                userDTO = userService.login(userDTO);
            }

            HashMap userMap = new HashMap();
            userMap.put("userId", userDTO.getUserId());
            userMap.put("userName", userDTO.getUserName());

            return ResponseEntity.ok()
                    .header("token", jwtService.createToken(userDTO))
                    .body(userMap);

        } catch (Exception | ErrorResponse e){
            log.error(e.getMessage());
            return new ResponseEntity<>("SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 개인 정보 수정 :: 비밀번호
     * @param userDTO
     * @return
     */
    @PutMapping(path = "/user/password")
    public ResponseEntity user(@RequestBody @Valid UserDTO userDTO){
        try{
            log.info("================================= user password update start");
            //비밀번호 길이 : 8 이상 500 이하
            if (userDTO.getPassword().length() < 8 || 500 < userDTO.getPassword().length() ){
                return new ResponseEntity<>("PASSWORD_LENGTH_MISMATCH", HttpStatus.BAD_REQUEST); //400
            }
            UserDTO updateUserDTO = userService.updatePassword(userDTO);
            return new ResponseEntity<>(updateUserDTO.getUserId(), HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e.getMessage().equals("NOT_FOUND_USER")) {
                return new ResponseEntity<>("NOT_FOUND_USER", HttpStatus.BAD_REQUEST);
            } else if (e.getMessage().equals("KAKAO_USER")) {
                return new ResponseEntity<>("KAKAO_USER", HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>("SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * 개인 정보 수정 :: 닉네임
     * @param userDTO
     * @return
     */
    @PutMapping(path = "/user/name")
    public ResponseEntity update(@RequestBody @Valid UserDTO userDTO){
        try{
            log.info("================================= user name update start");
            UserDTO updateUserDTO = userService.updateName(userDTO);
            return new ResponseEntity<>(updateUserDTO, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e.getMessage().equals("ALREADY_EXIST_USER_NAME")){
                return new ResponseEntity<>("ALREADY_EXIST_USER_NAME", HttpStatus.CONFLICT); //409 리소스 충돌
            } else if (e.getMessage().equals("NOT_FOUND_USER")){
                return new ResponseEntity<>("NOT_FOUND_USER", HttpStatus.BAD_REQUEST); //400
            } else {
                return new ResponseEntity<>("SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * 회원 탈퇴
     * @param userDTO
     * @return
     */
    @PostMapping(path = "/delete")
    public ResponseEntity delete(@RequestBody UserDTO userDTO){
        //탈퇴 성공시 성공코드 리턴
        try {
            log.info("================================= user delete start");
            String result = userService.delete(userDTO);
            if (result.equals("success")) {
                return new ResponseEntity<>("success", HttpStatus.OK);
            }
            return new ResponseEntity<>("SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new ResponseEntity<>("SERVER_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
