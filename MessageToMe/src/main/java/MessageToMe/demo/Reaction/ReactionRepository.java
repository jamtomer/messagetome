package MessageToMe.demo.Reaction;

import MessageToMe.demo.Message.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReactionRepository extends JpaRepository<Reaction, Long> {
//    List<Reaction> findAllByUser(Long userId);
    Optional<List<Reaction>> findReactionByMessage(Message message);
    Optional<Reaction> findReactionByReactionId(Long reactionId);
    void deleteReactionByReactionId(Long reactionId);

}
