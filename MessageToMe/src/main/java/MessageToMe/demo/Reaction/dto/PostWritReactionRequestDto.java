package MessageToMe.demo.Reaction.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostWritReactionRequestDto{
    UserDTO user;
    MessageDTO message;
    ReactionDTO reaction;
}
