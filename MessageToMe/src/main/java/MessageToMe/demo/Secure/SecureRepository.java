package MessageToMe.demo.Secure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SecureRepository extends JpaRepository<Secure, Long> {
    Secure findSecureBySecureKey(String key);
}
