package MessageToMe.demo.Secure;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Table(name="secure")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
public class Secure {

    @Id
    @NotNull
    @GeneratedValue
    @Column(name = "secure_key", unique = true)
    private String secureKey;

    @Column(name = "secure_code")
    private String secureCode;
}
