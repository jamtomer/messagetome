package MessageToMe.demo.Secure;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SecureDTO {
    private String secureKey;
    private String secureCode;
}
