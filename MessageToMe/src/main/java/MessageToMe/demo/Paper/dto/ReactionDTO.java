package MessageToMe.demo.Paper.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor (access = AccessLevel.PROTECTED)
public class ReactionDTO {
    private Long messageId;
    private Long reactionId;
    private String emoji;
    private String userName;
}
