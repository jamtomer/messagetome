package MessageToMe.demo.Paper.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor (access = AccessLevel.PROTECTED)
public class StickerDTO {
    private Long stickerId;
    private String positionX;
    private String positionY;
    private Long stickerType;
    private String userName;
    private Long userId;
}
