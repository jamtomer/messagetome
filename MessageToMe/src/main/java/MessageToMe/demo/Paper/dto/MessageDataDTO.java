package MessageToMe.demo.Paper.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor (access = AccessLevel.PROTECTED)
@Data
public class MessageDataDTO {
    private Long messageId;
    private String content;
    private String userName;
    private String color;
}
