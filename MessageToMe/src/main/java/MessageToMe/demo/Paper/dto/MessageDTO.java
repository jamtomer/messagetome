package MessageToMe.demo.Paper.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor (access = AccessLevel.PROTECTED)
public class MessageDTO {
    private Long messageId;
    private String color;
    private String content;
    private String email;
    private String userName;
    private char read;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
}
