package MessageToMe.demo.Paper.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor (access = AccessLevel.PROTECTED)
@Data
public class PaperListDTO {
    private Long paperId;
    private String paperTitle;
    private int skin;
    @JsonFormat (pattern = "yyyy-MM-dd")
    private LocalDateTime createDate;
    private char giftyn;
    private int messageCount;
    private List<MessageDataDTO> messages;
}
