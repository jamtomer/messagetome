package MessageToMe.demo.Paper;

import MessageToMe.demo.Exception.ErrorMessage;
import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Message.Message;
import MessageToMe.demo.Message.MessageRepository;
import MessageToMe.demo.Paper.dto.*;
import MessageToMe.demo.Reaction.Reaction;
import MessageToMe.demo.Reaction.ReactionRepository;
import MessageToMe.demo.Sticker.Sticker;
import MessageToMe.demo.Sticker.StickerRepository;
import MessageToMe.demo.User.User;
import MessageToMe.demo.User.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional (readOnly = true)
@RequiredArgsConstructor
public class PaperService {

    private final UserRepository userRepository;
    private final PaperRepository paperRepository;
    private final MessageRepository messageRepository;
    private final ReactionRepository reactionRepository;
    private final StickerRepository stickerRepository;

    @Transactional
    public Paper savePaper(long userId, PaperDTO paperDto) throws ErrorResponse {
        User findUser = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        try {
            Paper paper = new Paper();
            paper.setPaperTitle(paperDto.getPaperTitle());
            paper.setPaperSkin(paperDto.getSkin());
            paper.setDelyn('n');
            paper.setCreateDate(paperDto.getCreateDate());
            paper.setCreateUser(findUser);
            paper.setOwnerUser(findUser);
            paper.setGiftyn('n');

            checkTitleLength(paper.getPaperTitle());
            validateDuplicateTitle(paper.getPaperTitle());
            validateCreateDate(paper);

            return paperRepository.save(paper);
        } catch (Exception e){
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    private void checkTitleLength(String title) throws ErrorResponse {
        if (title.length() > 10 || title.length() < 1) {
            throw new ErrorResponse(ErrorMessage.NOT_VALID_PAPER_TITLE_LENGTH);
        }
    }

    private void validateDuplicateTitle(String title) throws ErrorResponse {
        boolean isExistPaperTitle = paperRepository.existsPaperByPaperTitleAndDelyn(title, 'n');
        if (isExistPaperTitle) {
            throw new ErrorResponse(ErrorMessage.DUPLICATE_PAPER_TITLE);
        }
    }

    private void validateCreateDate(Paper paper) throws ErrorResponse {
        LocalDateTime start = LocalDateTime.of(LocalDate.now(), LocalTime.of(0,0,0));
        LocalDateTime end = LocalDateTime.of(LocalDate.now(), LocalTime.of(23,59,59));
        boolean isExistPaper = paperRepository.existsPaperByCreateDateBetweenAndCreateUserAndDelyn(start, end, paper.getCreateUser(), 'n');
        if (isExistPaper) {
            throw new ErrorResponse(ErrorMessage.CREATE_PAPER_ONLY_ONCE_A_DAY);
        }
    }

    public boolean validateCreateDate(long userId) throws ErrorResponse {
        User findUser = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        LocalDateTime start = LocalDateTime.of(LocalDate.now(), LocalTime.of(0,0,0));
        LocalDateTime end = LocalDateTime.of(LocalDate.now(), LocalTime.of(23,59,59));
        boolean isExistPaper = paperRepository.existsPaperByCreateDateBetweenAndCreateUserAndDelyn(start, end, findUser, 'n');
        return isExistPaper;
    }

    public List<PaperListDTO> getPaperList(long userId, Pageable pageable) throws ErrorResponse {
        User findUser = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        List<Paper> findPapers = paperRepository.findPaperByOwnerUserAndDelynOrderByCreateDateDesc(findUser, 'n', pageable)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));
        List<PaperListDTO> collect = new ArrayList<>();

        try {
            for (Paper findPaper : findPapers) {
                Long paperId = findPaper.getPaperId();
                List<MessageDataDTO> mList = findMessageData(findPaper);
                PaperListDTO p = new PaperListDTO(
                        paperId,
                        findPaper.getPaperTitle(),
                        findPaper.getPaperSkin(),
                        findPaper.getCreateDate(),
                        findPaper.getGiftyn(),
                        mList.size(),
                        mList
                );
                collect.add(p);
            }
            return collect;
        } catch (Exception e){
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }

    public List<MessageDataDTO> findMessageData(Paper paper) throws ErrorResponse {
        try {
            List<Message> messageList = messageRepository.findTop5ByPaperAndDelynOrderByCreatedateDesc(paper, 'n')
                    .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_MESSAGE));
            return messageList.stream()
                    .map(m -> new MessageDataDTO(m.getMessageId(), m.getContent(), m.getUser().getUserName(), m.getColor())).collect(Collectors.toList());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }

    @Transactional
    public void updatePaper(long userId, PaperDTO paperDTO) throws ErrorResponse {
        Paper paper = paperRepository.findPaperByPaperId(paperDTO.getPaperId())
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));

        // 삭제된 페이퍼 수정하려고 접근할 때도 찾을 수 없는 페이퍼라고 리턴
        if (paper.getDelyn() == 'y') {
            throw new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER);
        }

        User findUser = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));

        if (!findUser.getUserId().equals(paper.getOwnerUser().getUserId())) {
            throw new ErrorResponse(ErrorMessage.NOT_VALID_USER);
        }

        try {
            String title = paperDTO.getPaperTitle();
            checkTitleLength(title);
            validateDuplicateTitle(title);

            paper.setPaperTitle(title);
            paper.setPaperSkin(paperDTO.getSkin());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    @Transactional
    public void deletePaper(long userId, Long paperId) throws ErrorResponse {
        User findUser = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Paper deletePaper = paperRepository.findPaperByPaperIdAndOwnerUser(paperId, findUser)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));

        if (!findUser.getUserId().equals(deletePaper.getOwnerUser().getUserId())) {
            throw new ErrorResponse(ErrorMessage.NOT_VALID_USER);
        }

        try {
            deletePaper.setDelyn('y');
            deletePaper.setDeleteDate(LocalDateTime.now());
            deletePaper.setPaperTitle("삭제된 종이입니다.");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    public ResponsePaperDetailListDTO getPaperDetail(long userId, long paperId) throws ErrorResponse {
        User findUser = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Paper findPaper = paperRepository.findPaperByPaperIdAndDelyn(paperId, 'n')
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));
        try {
            PaperDetailDTO paperDetailDTO = new PaperDetailDTO(findPaper.getPaperTitle(), findPaper.getPaperSkin());
            updateMessageReadyn(findUser, findPaper);
            List<MessageDTO> messageList = getMessageList(findPaper);
            List<StickerDTO> stickerList = getStickerList(findPaper);
            List<ReactionDTO> reactionList = getReactionList(findPaper);
            return new ResponsePaperDetailListDTO(paperDetailDTO, messageList, stickerList, reactionList);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }

    public ResponsePaperDetailListDTO getPaperDetail(long paperId) throws ErrorResponse {
        Paper findPaper = paperRepository.findPaperByPaperIdAndDelyn(paperId, 'n')
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));
        try {
            PaperDetailDTO paperDetailDTO = new PaperDetailDTO(findPaper.getPaperTitle(), findPaper.getPaperSkin());
            List<MessageDTO> messageList = getMessageList(findPaper);
            List<StickerDTO> stickerList = getStickerList(findPaper);
            List<ReactionDTO> reactionList = getReactionList(findPaper);
            return new ResponsePaperDetailListDTO(paperDetailDTO, messageList, stickerList, reactionList);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }

    public List<MessageDTO> getMessageList(Paper paper) throws ErrorResponse {
        List<Message> messageList = messageRepository.findAllByPaperAndDelynOrderByCreatedateDesc(paper, 'n')
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_MESSAGE));
        try {
            return messageList.stream()
                    .map(m -> new MessageDTO(m.getMessageId(), m.getColor(), m.getContent(), m.getUser().getEmail(), m.getUser().getUserName(), m.getReadyn(), m.getCreatedate())).collect(Collectors.toList());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }

    @Transactional
    public void updateMessageReadyn(User user, Paper paper) throws ErrorResponse {
        try {
            if (user.getUserId().equals(paper.getOwnerUser().getUserId())) {
                messageRepository.updateMessageReadyn(paper, 'y');
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    public List<ReactionDTO> getReactionList(Paper paper) throws ErrorResponse {
        List<Message> findMessage = messageRepository.findAllByPaperAndDelyn(paper, 'n')
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_MESSAGE));
        List<ReactionDTO> collect = new ArrayList<>();
        try {
            for (Message message : findMessage) {
                List<Reaction> reactionList = reactionRepository.findReactionByMessage(message)
                        .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_REACTION));
                List<ReactionDTO> temp = reactionList.stream()
                        .map(m -> new ReactionDTO(m.getMessage().getMessageId(), m.getReactionId(), m.getEmoji(), m.getUser().getUserName())).collect(Collectors.toList());
                collect.addAll(temp);
            }
            return collect;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }

    public List<StickerDTO> getStickerList(Paper paper) throws ErrorResponse {
        List<Sticker> stickerList = stickerRepository.findAllByPaper(paper)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_STICKER));
        try {
            return stickerList.stream()
                    .map(m -> new StickerDTO(m.getStickerId(), m.getPositionX(), m.getPositionY(), m.getKind(), m.getUser().getUserName(), m.getUser().getUserId())).collect(Collectors.toList());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }

    @Transactional
    public void sendPaper(long userId, String recipientName, long paperId) throws ErrorResponse {
        User sender = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        User recipient = userRepository.findUserByUserName(recipientName)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Paper findPaper = paperRepository.findPaperByPaperIdAndDelyn(paperId, 'n')
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));

        if (sender.getUserId().equals(recipient.getUserId())) {
            throw new ErrorResponse(ErrorMessage.NOT_ALLOW_SELF_GIFT);
        }

        if (!sender.getUserId().equals(findPaper.getCreateUser().getUserId())){
            throw new ErrorResponse(ErrorMessage.NOT_VALID_USER);
        }

        try {
            findPaper.setGiftyn('y');
            findPaper.setOwnerUser(recipient);

            Paper paper = paperRepository.findPaperByPaperIdAndDelyn(paperId, 'n')
                    .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));

            // 페이퍼 선물하기로 소유자가 받는 사람인 recipient로 바뀐다면
            // 페이퍼에 있는 메시지들의 읽음여부(readyn 컬럼) 값을 모두 읽지 않음('n') 처리 한다.
            if(recipient.getUserId().equals(paper.getOwnerUser().getUserId())) {
                messageRepository.updateMessageReadyn(findPaper, 'n');
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

}
