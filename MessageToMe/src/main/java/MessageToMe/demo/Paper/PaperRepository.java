package MessageToMe.demo.Paper;

import MessageToMe.demo.User.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PaperRepository extends JpaRepository<Paper, Long> {

    Optional<List<Paper>> findPaperByOwnerUserAndDelynOrderByCreateDateDesc(User user, char delyn, Pageable pageable);
    Optional<Paper> findPaperByPaperId(Long paperId);
    Optional<Paper> findPaperByPaperIdAndDelyn(Long paperId, char n);
    Optional<Paper> findPaperByPaperIdAndOwnerUser(Long paperId, User user);
    Boolean existsPaperByPaperTitleAndDelyn(String title, char n);
    Boolean existsPaperByCreateDateBetweenAndCreateUserAndDelyn(LocalDateTime start, LocalDateTime end, User user, char n);

}
