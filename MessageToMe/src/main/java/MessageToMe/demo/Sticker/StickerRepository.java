package MessageToMe.demo.Sticker;

import MessageToMe.demo.Paper.Paper;
import MessageToMe.demo.User.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StickerRepository extends JpaRepository<Sticker, Long> {

    Optional<List<Sticker>> findAllByUser(User user);
    Optional<List<Sticker>> findAllByPaper(Paper paper);
    Optional<Sticker> findStickerByStickerId(long stickerId);

    @Query(value = "SELECT COUNT(s) FROM Sticker s WHERE s.paper = ?1")
    Integer countStickerByPaper(Paper paper);

    boolean existsStickerByUserAndPaper(User user, Paper paper);
}
