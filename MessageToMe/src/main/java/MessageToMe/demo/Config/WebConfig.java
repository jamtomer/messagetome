package MessageToMe.demo.Config;

import MessageToMe.demo.Interceptor.MessageToMeInterceptor;
import MessageToMe.demo.User.JWTService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    /**
     * CORS 진입 설정
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:8833", "http://localhost:3307", "https://www.byeolmal.today")
                .allowedMethods("GET", "POST", "PUT", "DELETE");
    }

    /**
     * Interceptor 진입 설정
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(messageToMeInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/login", "/join"
                        //, "/user/name", "/user/password", "/delete"
                        , "/validateEmail", "/validateName", "/checkEmail");

    }

    @Bean
    public MessageToMeInterceptor messageToMeInterceptor() {
        return new MessageToMeInterceptor(new JWTService());
    }

}
