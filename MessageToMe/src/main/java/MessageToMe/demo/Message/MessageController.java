package MessageToMe.demo.Message;
import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Message.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Validated
@Slf4j
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService){
        this.messageService = messageService;
    }

    @PostMapping(path = "/message")
    public ResponseEntity<?> writeMessage(@RequestBody @Valid PostWriteMessageRequestDto postWriteMessageRequestDto) {
        try{
            Long savedId = messageService.save(
                    postWriteMessageRequestDto.getUser().getUserId(),
                    postWriteMessageRequestDto.getPaper().getPaperId(),
                    postWriteMessageRequestDto.getMessage());
            return new ResponseEntity<>("SUCCESS", HttpStatus.NO_CONTENT);
        } catch (ErrorResponse e) {
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @PutMapping(path = "/message/{messageId}")
    public ResponseEntity<?> updateMessage(@PathVariable String messageId, @RequestBody @Valid PutUpdateMessageRequestDto putUpdateMessageRequestDto){
        try{
            Long parseMessageId = Long.parseLong(messageId);
            messageService.update(
                parseMessageId,
                putUpdateMessageRequestDto.getUser().getUserId(),
                putUpdateMessageRequestDto.getMessage());
            return new ResponseEntity<>("SUCCESS", HttpStatus.NO_CONTENT);
        }catch (ErrorResponse e){
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }

    @DeleteMapping(path = "/message/{messageId}")
    public ResponseEntity<?> deleteMessage(@PathVariable String messageId,
                                        @RequestBody DeleteMessageRequestDto deleteMessageRequestDto) {
        try{
            Long parseMessageId = Long.parseLong(messageId);
            messageService.delete(
                    parseMessageId,
                    deleteMessageRequestDto.getUser().getUserId());
            return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
        }
        catch (ErrorResponse e){
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }

    }

    @GetMapping(path = "/message")
    public ResponseEntity<?> getMessageList(@RequestHeader("User-Id") Long userId) {
        try{
            GetMessageListResponseDto getMessageListResponseDto =
                    messageService.retrieveAll(userId);
            return new ResponseEntity<>(getMessageListResponseDto, HttpStatus.OK);
        }
        catch(ErrorResponse e){
            log.error(e.getMessage());
            return new ResponseEntity<>(e.getErrorMessage().getMessage(),
                    HttpStatus.valueOf(e.getErrorMessage().getStatus()));
        }
    }
}
