package MessageToMe.demo.Message.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeleteMessageRequestDto {
    MessageDTO message;
    UserDTO user;
}
