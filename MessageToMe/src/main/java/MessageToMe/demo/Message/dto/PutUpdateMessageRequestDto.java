package MessageToMe.demo.Message.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class PutUpdateMessageRequestDto {
    UserDTO user;
    @Valid
    MessageDTO message;
}
