package MessageToMe.demo.Message.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Valid
public class MessageDTO {
    private Long messageId;
    private String font;
    private String color;
    @Size(min= 1)
    @Size(max= 420)
    private String content;
    private String userName;
    private char readny;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Seoul")
    private LocalDateTime createDate;
    private Long paperId;
    private String paperTitle;
}
