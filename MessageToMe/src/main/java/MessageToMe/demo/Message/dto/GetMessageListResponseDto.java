package MessageToMe.demo.Message.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetMessageListResponseDto {
    List<MessageDTO> messages;
}
