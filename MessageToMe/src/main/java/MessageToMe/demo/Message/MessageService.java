package MessageToMe.demo.Message;

import MessageToMe.demo.Exception.ErrorMessage;
import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Message.dto.*;
import MessageToMe.demo.Paper.PaperRepository;
import MessageToMe.demo.User.User;
import MessageToMe.demo.User.UserRepository;
import MessageToMe.demo.Paper.Paper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
@Slf4j
public class MessageService {

    private final MessageRepository messageRepository;
    private final UserRepository userRepository;
    private final PaperRepository paperRepository;

    @Transactional
    public Long save(Long userId, Long paperId, MessageDTO messageDTO) throws ErrorResponse {
        User user = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Paper paper = paperRepository.findPaperByPaperId(paperId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_PAPER));

        Optional<List<Message>> existingMessages = messageRepository.findAllByPaperAndDelyn(paper, 'n');
        if (existingMessages.isPresent() && existingMessages.get().size() > 420){
            throw new ErrorResponse(ErrorMessage.TOO_MANY_EXISTING_MESSAGES);
        }

        try{
            Message message = Message.builder()
                    .content(messageDTO.getContent())
                    .font(messageDTO.getFont())
                    .color(messageDTO.getColor())
                    .delyn('n')
                    .readyn('n')
                    .user(user)
                    .paper(paper)
                    .build();
            Long savedId = messageRepository.save(message).getMessageId();
            return savedId;
        } catch (Exception e){
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    @Transactional
    public ResponseEntity<?> update(Long messageId, Long userId, MessageDTO messageDTO) throws ErrorResponse{
        User user = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Message message = messageRepository.findMessageByMessageIdAndDelyn(messageId, 'n')
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_MESSAGE));


        if (!user.getUserId().equals(message.getUser().getUserId())){
            log.error(String.valueOf(user.getUserId()));
            log.error(String.valueOf(message.getUser().getUserId()));
            throw new ErrorResponse(ErrorMessage.NOT_VALID_USER);
        }

        try {
            message.update(
                    messageDTO.getContent(),
                    messageDTO.getFont(),
                    messageDTO.getColor());
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    @Transactional
    public ResponseEntity<?> delete(Long messageId, Long userId) throws ErrorResponse{
        User user = userRepository.findUserByUserId(userId)
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));
        Message message = messageRepository.findMessageByMessageIdAndDelyn(messageId, 'n')
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_MESSAGE));

        if (!user.getUserId().equals(message.getUser().getUserId())){
            throw new ErrorResponse(ErrorMessage.NOT_VALID_USER);
        }

        try{
            message.delete('y');
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e){
            throw new ErrorResponse(ErrorMessage.DB_SAVING);
        }
    }

    @Transactional
    public GetMessageListResponseDto retrieveAll(Long userId) throws ErrorResponse {
        User user = userRepository.findUserByUserId(userId)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_USER));

        // Request를 요청한 유저가 작성한 메시지 목록을 리턴합니다.
        List<Message> messageList = messageRepository.findAllWithPaperTitle(user)
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_MESSAGE));

        System.out.println(messageList);
        GetMessageListResponseDto getMessageListResponseDto = new GetMessageListResponseDto();

        try{
            List<MessageDTO> messageDTOList = getMessageDTOList(messageList);
            getMessageListResponseDto.setMessages(messageDTOList);
            return getMessageListResponseDto;
        }catch (Exception e){
            throw new ErrorResponse(ErrorMessage.SERVER);
        }
    }

    public List<MessageDTO> getMessageDTOList(List<Message> messageList){
        List<MessageDTO> messageDTOList = messageList.stream().map(m ->
            new MessageDTO(
                m.getMessageId(),
                m.getFont(),
                m.getColor(),
                m.getContent(),
                m.getUser().getUserName(),
                m.getReadyn(),
                m.getCreatedate(),
                m.getPaper().getPaperId(),
                m.getPaper().getPaperTitle()
            )).collect(Collectors.toList());
        return messageDTOList;
    }
}

