package MessageToMe.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.TimeZone;

@EnableJpaAuditing
@SpringBootApplication
public class JamToMerApplication {

	@PostConstruct
	public void started() {
		// 서버 timezone KST 셋팅했습니다. - jam
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Seoul"));
		System.out.println(new Date().toString());
	}
	public static void main(String[] args) {
		SpringApplication.run(JamToMerApplication.class, args);
	}

}
