# DROP TABLE if exists user;
#
# CREATE TABLE user
# (
# `user_id` bigint(20) NOT NULL,
# `auth_num` int(11) DEFAULT NULL,
# `create_date` datetime(6) DEFAULT CURRENT_TIMESTAMP,
# `email` varchar(255) DEFAULT NULL,
# `password` varchar(255) DEFAULT NULL,
# `user_name` varchar(255) DEFAULT NULL,
# PRIMARY KEY (`user_id`),
# UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
# UNIQUE KEY `UK_lqjrcobrh9jc8wpcar64q1bfh` (`user_name`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#
#
# DROP TABLE if exists paper;
#
# CREATE TABLE paper (
#  `paper_id` bigint(20) NOT NULL,
#  `create_date` datetime(6) DEFAULT NULL,
#  `delete_date` datetime(6) DEFAULT NULL,
#  `delyn` char(1) DEFAULT NULL,
#  `paper_skin` int(11) DEFAULT NULL,
#  `paper_title` varchar(255) DEFAULT NULL,
#  `user_id` bigint(20) DEFAULT NULL,
#  PRIMARY KEY (`paper_id`),
#  KEY `FKpom80prxg2k1l4hlvlxyr0lhq` (`user_id`),
# CONSTRAINT `FKpom80prxg2k1l4hlvlxyr0lhq` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#
#
# DROP TABLE if exists message;
#
# CREATE TABLE message (
# `message_id` bigint(20) NOT NULL,
# `color` varchar(255) DEFAULT NULL,
# `content` text NOT NULL,
# `createdate` datetime(6) DEFAULT NULL,
# `deletedate` datetime(6) DEFAULT NULL,
# `delyn` char(1) DEFAULT NULL,
# `font` varchar(255) DEFAULT NULL,
# `readyn` char(1) DEFAULT NULL,
# `user_id` bigint(20) DEFAULT NULL,
# `paper_id` bigint(20) DEFAULT NULL,
# PRIMARY KEY (`message_id`),
# KEY `FKb3y6etti1cfougkdr0qiiemgv` (`user_id`),
# KEY `FK1guxfgnvqij19qs6aokf72u4u` (`paper_id`),
# CONSTRAINT `FK1guxfgnvqij19qs6aokf72u4u` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`),
# CONSTRAINT `FKb3y6etti1cfougkdr0qiiemgv` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#
#
# DROP TABLE if exists reaction;
#
# CREATE TABLE reaction (
# `reaction_id` bigint(20) NOT NULL,
# `emoji` char(3) DEFAULT NULL,
# `message_id` bigint(20) DEFAULT NULL,
# `user_id` bigint(20) DEFAULT NULL,
# PRIMARY KEY (`reaction_id`),
# KEY `FKnnwfsi01kkhg9o9woxilp9dep` (`message_id`),
# KEY `FKp68qgeq3telx6adl7hssrdxbw` (`user_id`),
# CONSTRAINT `FKnnwfsi01kkhg9o9woxilp9dep` FOREIGN KEY (`message_id`) REFERENCES `message` (`message_id`),
# CONSTRAINT `FKp68qgeq3telx6adl7hssrdxbw` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
#
# DROP TABLE if exists sticker;
#
# CREATE TABLE sticker (
# `sticker_id` bigint(20) NOT NULL,
# 'kind' bigint(20) NOT NULL,
# `positionx` varchar(255) DEFAULT NULL,
# `positiony` varchar(255) DEFAULT NULL,
# `user_id` bigint(20) DEFAULT NULL,
# `paper_id` bigint(20) DEFAULT NULL,
# PRIMARY KEY (`sticker_id`),
# KEY `FK4ailuxu7sqrt665mv0pic65ku` (`user_id`),
# KEY `FKn8lh25fcsjoucu81vnbrm0ais` (`paper_id`),
# CONSTRAINT `FK4ailuxu7sqrt665mv0pic65ku` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
# CONSTRAINT `FKn8lh25fcsjoucu81vnbrm0ais` FOREIGN KEY (`paper_id`) REFERENCES `paper` (`paper_id`)
# ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
