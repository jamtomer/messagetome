INSERT INTO user (user_id, auth_num, create_date, email, password, user_name)
VALUES
(0, 123, NOW(), 'haeun@gmail.com', 'haeun1234', 'haeun'),
(1, 123, NOW(), 'jam@gmail.com', 'jam1234', 'jam'),
(2, 123, NOW(), 'merege@gmail.com', 'merge1234', 'merge'),
(3, 123, NOW(), 'lanto@gmail.com', 'lanto1234', 'lanto');

INSERT INTO paper (paper_id, create_date, delete_date, delyn, paper_skin, paper_title, user_id)
VALUES
(0, NOW(), NULL, 'n', 1, 'lanto\'s paper!', 3),
(1, NOW(), NULL, 'n', 2, 'merge\'s paper!', 2),
(2, NOW(), NULL, 'n', 1, 'jam\'s paper!', 1),
(3, NOW(), NULL, 'n', 1, 'jam\'s second paper!', 1);


INSERT INTO MessageToMe.message (color, content, delyn, font, readyn, user_id, paper_id, create_date)
VALUES
('#111111', '잼님 안녕하세용 저는 하은이에요!', 'n', '굴림', 'n', 0, 1, NOW()),
('#111111', '머지님 안녕하세요! 저는 잼이에용', 'n', '굴림', 'n', 1, 2, NOW()),
('#111111', '머지님 안녕하세요! 저는 란토에요!', 'n', '궁서', 'n', 3, 2, NOW()),
('#111111', '잼님 안녕하세요 저는 란토에요!',  'n', '궁서', 'n', 3, 1, NOW());

INSERT INTO reaction (emoji, message_id, user_id)
VALUES
    ('❤️', 1, 1),
    ('😘', 1, 2),
    ('❤️', 2, 1),
    ('😘', 2, 2),
    ('😋', 3, 1);

INSERT INTO sticker (kind, positionx, positiony, user_id, paper_id)
VALUES
(1, '100', '100', 1, 1),
(2, '100', '100', 1, 2),
(3, '200', '200', 2, 2);
