package MessageToMe.demo.Sticker;

import MessageToMe.demo.Exception.ErrorMessage;
import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Message.MessageRepository;
import MessageToMe.demo.Paper.Paper;
import MessageToMe.demo.Paper.PaperRepository;
import MessageToMe.demo.Sticker.dto.StickerDTO;
import MessageToMe.demo.User.User;
import MessageToMe.demo.User.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@Transactional
public class StickerServiceTest {

    private User user;
    private Paper paper;

    @Autowired
    StickerService stickerService;
    
    @Autowired
    UserRepository userRepository;
    @Autowired
    PaperRepository paperRepository;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    StickerRepository stickerRepository;
    
    @BeforeEach
    void 유저_생성() {
        //given
        user = new User();
        user.setUserName("lanto_test");
        user.setEmail("lanto_test@test.com");
        user.setAuthNum(1);
        user.setPassword("lanto1234");
        user.setCreateDate(LocalDateTime.now());
        this.user = userRepository.save(user);
    }

    @BeforeEach
    void 페이퍼_생성() {
        paper = new Paper();
        paper.setPaperTitle("test~!!!");
        paper.setPaperSkin(5);
        paper.setCreateUser(this.user);
        paper.setCreateDate(LocalDateTime.of(2022, 1, 2, 12, 34,56,7890));
        this.paper = paperRepository.save(paper);
    }

    @Test
    void 스티커_생성_테스트() throws ErrorResponse {
        //given
        long userId = user.getUserId();
        StickerDTO stickerDTO = new StickerDTO();
        stickerDTO.setPaperId(paper.getPaperId());
        stickerDTO.setUserId(user.getUserId());
        stickerDTO.setKind(3L);
        stickerDTO.setPositionX("800");
        stickerDTO.setPositionY("200");

        //when
        Sticker saveSticker = stickerService.saveSticker(userId, stickerDTO);

        //then
        Sticker findSticker = stickerRepository.findStickerByStickerId(saveSticker.getStickerId())
                .orElseThrow(()-> new ErrorResponse(ErrorMessage.NOT_FOUND_STICKER));
        Assertions.assertEquals(findSticker.getUser().getUserId(), stickerDTO.getUserId());
        Assertions.assertEquals(findSticker.getPaper().getPaperId(), stickerDTO.getPaperId());
        Assertions.assertEquals(findSticker.getPositionX(), stickerDTO.getPositionX());
        Assertions.assertEquals(findSticker.getPositionY(), stickerDTO.getPositionY());
    }

    @Test
    void 스티커_삭제_테스트() throws ErrorResponse {
        //given
        long userId = user.getUserId();
        StickerDTO stickerDTO = new StickerDTO();
        stickerDTO.setPaperId(paper.getPaperId());
        stickerDTO.setUserId(user.getUserId());
        stickerDTO.setKind(3L);
        stickerDTO.setPositionX("800");
        stickerDTO.setPositionY("200");
        Sticker saveSticker = stickerService.saveSticker(userId, stickerDTO);
        stickerDTO.setStickerId(saveSticker.getStickerId());

        //when
        stickerService.deleteSticker(userId, stickerDTO);

        //then
        Optional<Sticker> optionalSticker = stickerRepository.findStickerByStickerId(saveSticker.getStickerId());
        Assertions.assertTrue(optionalSticker.isEmpty());
    }

    @Test
    void 스티커_삭제_실패_테스트() throws ErrorResponse {
        //given
        long userId = user.getUserId();
        StickerDTO stickerDTO = new StickerDTO();
        stickerDTO.setUserId(user.getUserId());
        stickerDTO.setPaperId(paper.getPaperId());
        stickerDTO.setKind(3L);
        stickerDTO.setPositionX("800");
        stickerDTO.setPositionY("200");
        Sticker saveSticker = stickerService.saveSticker(userId, stickerDTO);
        stickerDTO.setStickerId(saveSticker.getStickerId());

        Paper paper1 = new Paper();
        paper1.setPaperTitle("test~!!!!!");
        paper1.setPaperSkin(3);
        paper1.setCreateUser(this.user);
        paper1.setCreateDate(LocalDateTime.of(2022, 1, 3, 12, 34,56,7890));
        Long savePaperId = paperRepository.save(paper1).getPaperId();

        //when
        StickerDTO stickerDTO1 = new StickerDTO();
        stickerDTO1.setUserId(user.getUserId());
        stickerDTO1.setPaperId(savePaperId);
        stickerDTO1.setStickerId(saveSticker.getStickerId());

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> stickerService.deleteSticker(userId, stickerDTO1));
    }

    @Test
    void 스티커_수정_테스트() throws ErrorResponse {
        //given
        long userId = user.getUserId();
        StickerDTO stickerDTO = new StickerDTO();
        stickerDTO.setPaperId(paper.getPaperId());
        stickerDTO.setUserId(user.getUserId());
        stickerDTO.setKind(3L);
        stickerDTO.setPositionX("800");
        stickerDTO.setPositionY("200");
        Sticker saveSticker = stickerService.saveSticker(userId, stickerDTO);
        stickerDTO.setStickerId(saveSticker.getStickerId());

        //when
        String nextX = "700";
        String nextY = "500";
        long newKind = 22L;
        stickerDTO.setPositionX(nextX);
        stickerDTO.setPositionY(nextY);
        stickerDTO.setKind(newKind);
        stickerService.updateSticker(userId, stickerDTO);

        //then
        Sticker findSticker = stickerRepository.findStickerByStickerId(saveSticker.getStickerId())
                .orElseThrow(() -> new ErrorResponse(ErrorMessage.NOT_FOUND_STICKER));
        Assertions.assertEquals(user.getUserId(), findSticker.getUser().getUserId());
        Assertions.assertEquals(nextX, findSticker.getPositionX());
        Assertions.assertEquals(nextY, findSticker.getPositionY());
        Assertions.assertEquals(newKind, findSticker.getKind());
    }

    @Test
    void 스티커_수정_실패_테스트() throws ErrorResponse {
        //given
        long userId = user.getUserId();
        StickerDTO stickerDTO = new StickerDTO();
        stickerDTO.setUserId(user.getUserId());
        stickerDTO.setPaperId(paper.getPaperId());
        stickerDTO.setKind(3L);
        stickerDTO.setPositionX("800");
        stickerDTO.setPositionY("200");
        Sticker saveSticker = stickerService.saveSticker(userId, stickerDTO);
        stickerDTO.setStickerId(saveSticker.getStickerId());

        Paper paper1 = new Paper();
        paper1.setPaperTitle("test~!!!!!");
        paper1.setPaperSkin(3);
        paper1.setCreateUser(this.user);
        paper1.setCreateDate(LocalDateTime.of(2022, 1, 3, 12, 34,56,7890));
        Long savePaperId = paperRepository.save(paper1).getPaperId();

        //when
        StickerDTO stickerDTO1 = new StickerDTO();
        stickerDTO1.setUserId(user.getUserId());
        stickerDTO1.setPaperId(savePaperId);
        stickerDTO1.setStickerId(saveSticker.getStickerId());

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> stickerService.updateSticker(userId, stickerDTO1));
    }

    @Test
    void 스티커는_한_페이퍼당_140개를_초과할_수_없다() {
        //given
        List<User> userList = new ArrayList<>();
        for (int i = 0; i < 140; i++) {
            String email = "stickerCreateUser" + i + "@gmail.com";
            User createUser = new User();
            createUser.setEmail(email);
            createUser.setPassword("test1234");
            createUser.setUserName("stickerCreateUser" + i);
            createUser.setAuthNum(1);
            createUser.setCreateDate(LocalDateTime.now());
            userList.add(createUser);
        }
        userRepository.saveAll(userList);

        List<Sticker> stickerList = new ArrayList<>();
        for (int i = 0; i < 140; i++) {
            Sticker createSticker = new Sticker();
            createSticker.setPaper(paper);
            createSticker.setUser(userList.get(i));
            createSticker.setKind(3L);
            createSticker.setPositionX("800" + i);
            createSticker.setPositionY("200" + i);
            stickerList.add(createSticker);
        }
        stickerRepository.saveAll(stickerList);

        //when
        long userId = user.getUserId();
        StickerDTO stickerDTO = new StickerDTO();
        stickerDTO.setUserId(user.getUserId());
        stickerDTO.setPaperId(paper.getPaperId());
        stickerDTO.setKind(3L);
        stickerDTO.setPositionX("300");
        stickerDTO.setPositionY("400");

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> stickerService.saveSticker(userId, stickerDTO));
    }

    @Test
    void 한_페이퍼에는_한_사람당_스티커_한_개_제한() throws ErrorResponse {
        //given
        long userId = user.getUserId();
        StickerDTO stickerDTO = new StickerDTO();
        stickerDTO.setPaperId(paper.getPaperId());
        stickerDTO.setUserId(user.getUserId());
        stickerDTO.setKind(3L);
        stickerDTO.setPositionX("800");
        stickerDTO.setPositionY("200");

        //when
        stickerService.saveSticker(userId, stickerDTO);

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> stickerService.saveSticker(userId, stickerDTO));
    }

}
