package MessageToMe.demo.Reaction;

import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Message.Message;
import MessageToMe.demo.Message.MessageController;
import MessageToMe.demo.Message.MessageRepository;
import MessageToMe.demo.Message.MessageService;
import MessageToMe.demo.Message.dto.MessageDTO;
import MessageToMe.demo.Paper.Paper;
import MessageToMe.demo.Paper.PaperRepository;
import MessageToMe.demo.User.User;
import MessageToMe.demo.User.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
class ReactionControllerTest {

    private User lanto;
    private User merge;
    private User jam;
    private Long lantoId;
    private Long mergeId;
    private Long jamId;
    private Paper paper;
    private Message message;
    private Long paperId;
    private Long messageId;

    @Autowired
    ReactionService reactionService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PaperRepository paperRepository;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    ReactionRepository reactionRepository;

    @BeforeEach
    void 페이퍼_소유자_유저_생성(){
        lanto = new User();
        lanto.setUserName("lantoTest");
        lanto.setEmail("lantoTest@test.com");
        lanto.setAuthNum(1);
        lanto.setPassword("lanto1234");
        lanto.setCreateDate(LocalDateTime.now());
        lanto = userRepository.save(lanto);
        lantoId = lanto.getUserId();

    }

    @BeforeEach
    void 메세지_작성자_유저_생성(){
        merge = new User();
        merge.setUserName("mergeTest");
        merge.setEmail("mergeTest@test.com");
        merge.setAuthNum(1);
        merge.setPassword("merge1234");
        merge.setCreateDate(LocalDateTime.now());
        merge = userRepository.save(merge);
        mergeId = merge.getUserId();
    }

    @BeforeEach
    void 리액션_작성자_유저_생성(){
        jam = new User();
        jam.setUserName("jamTest");
        jam.setEmail("jamTest@test.com");
        jam.setAuthNum(1);
        jam.setPassword("jam1234");
        jam.setCreateDate(LocalDateTime.now());
        jam = userRepository.save(jam);
        jamId = jam.getUserId();
    }

    @BeforeEach
    void 페이퍼_생성(){
        paper = new Paper();
        paper.setPaperTitle("Lanto's Test Paper");
        paper.setPaperSkin(0);
        paper.setDelyn('n');
        paper.setCreateDate(LocalDateTime.now());
        paper.setOwnerUser(lanto);
        paper = paperRepository.save(paper);
        paperId = paper.getPaperId();
    }

    @BeforeEach
    void 메세지_작성(){
        message = Message.builder()
                .content("리액션 작성 테스트")
                .font("굴림체")
                .color("#123456")
                .delyn('n')
                .readyn('n')
                .user(merge)
                .paper(paper)
                .build();
        messageId = messageRepository.save(message).getMessageId();
    }

    @Test
    void 리액션_작성() throws ErrorResponse {
        //given - lanto의 paper에 작성한 merge의 message에 jam이 reaction을 작성하는 경우

        // when
        Long reacitonId = reactionService.save("❤️", jamId, messageId);

        // then
        Reaction reaction = reactionRepository.findReactionByReactionId(reacitonId)
                .orElseThrow(() -> new IllegalArgumentException("리액션 작성 테스트 실패"));

        Assertions.assertThat(reaction.getReactionId()).isEqualTo(reacitonId);
        Assertions.assertThat(reaction.getEmoji()).isEqualTo("❤️");
    }

    @Test
    void 리액션_수정() throws ErrorResponse {
        //given - lanto의 paper에 작성한 merge의 message에 jam이 reaction을 작성하고 jam이 수정하는 경우

        Long reacitonId = reactionService.save("❤️", jamId, messageId);

        // when
        reactionService.update(jamId, reacitonId, "💟");

        // then
        Reaction reaction = reactionRepository.findReactionByReactionId(reacitonId)
                .orElseThrow(() -> new IllegalArgumentException("리액션 수정 테스트 실패"));

        Assertions.assertThat(reaction.getReactionId()).isEqualTo(reacitonId);
        Assertions.assertThat(reaction.getEmoji()).isEqualTo("💟");
    }

    @Test
    void 리액션_삭제() throws ErrorResponse {
        //given - lanto의 paper에 작성한 merge의 message에 jam이 reaction을 작성하고 jam이 삭제하는 경우

        Long reacitonId = reactionService.save("❤️", jamId, messageId);

        // when
        reactionService.delete(jamId, reacitonId);

        // then
        Optional<Reaction> reaction = reactionRepository.findReactionByReactionId(reacitonId);
        Assertions.assertThat(reaction).isEqualTo(Optional.empty());
    }
}