package MessageToMe.demo.Message;

import MessageToMe.demo.Message.dto.*;
import MessageToMe.demo.Paper.PaperRepository;
import MessageToMe.demo.User.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@SpringBootTest
@Transactional
class MessageControllerTest {

    private Long paperId = 1L;
    private String paperTitle = "jam's test paper";
    private String userEmail = "jam@test.com";
    private Long userId = 1L;
    private String messageId = "1";
    private PostWriteMessageRequestDto postWriteMessageRequestDto;
    private PutUpdateMessageRequestDto putUpdateMessageRequestDto;

    @Autowired
    MessageController messageController;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PaperRepository paperRepository;
    @Autowired
    Validator validator;

    @Test
    void 메시지_작성_420자_초과_테스트(){
        // given
        postWriteMessageRequestDto = new PostWriteMessageRequestDto();

        // when
        postWriteMessageRequestDto.setUser(new UserDTO(userId));
        postWriteMessageRequestDto.setPaper(new PaperDTO(paperId, paperTitle));
        String contents =  "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트";
        postWriteMessageRequestDto.setMessage(new MessageDTO(null, "굴림체", "#111111", contents, null, 'n', null, paperId, paperTitle));

        //then
        Assertions.assertThatThrownBy(() -> {
            messageController.writeMessage(postWriteMessageRequestDto);
        }).isInstanceOf(ConstraintViolationException.class);

    }

    @Test
    void 메시지_작성_1자_미만_테스트() {
        // given
        postWriteMessageRequestDto = new PostWriteMessageRequestDto();

        // when
        postWriteMessageRequestDto.setUser(new UserDTO(userId));
        postWriteMessageRequestDto.setPaper(new PaperDTO(paperId, paperTitle));
        String contents =  "";
        postWriteMessageRequestDto.setMessage(new MessageDTO(null, "굴림체", "#111111", contents, null, 'n', null, paperId, paperTitle));

        //then
        Assertions.assertThatThrownBy(() -> {
            messageController.writeMessage(postWriteMessageRequestDto);
        }).isInstanceOf(ConstraintViolationException.class);
    }

    @Test
    void 메시지_수정_420자_초과_테스트() {
        // given
         putUpdateMessageRequestDto = new PutUpdateMessageRequestDto();

        // when
        putUpdateMessageRequestDto.setUser(new UserDTO(userId));
        String contents =  "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 " +
                "글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트 글자수 420자 초과 테스트";
        putUpdateMessageRequestDto.setMessage(new MessageDTO(null, "굴림체", "#111111", contents, null, 'n', null, paperId, paperTitle));

        //then
        Assertions.assertThatThrownBy(() -> {
            messageController.updateMessage(messageId, putUpdateMessageRequestDto);
        }).isInstanceOf(ConstraintViolationException.class);
    }

    @Test
    void 메시지_수정_1자_미만_테스트() {
        // given
        putUpdateMessageRequestDto = new PutUpdateMessageRequestDto();

        // when
        putUpdateMessageRequestDto.setUser(new UserDTO(userId));
        String contents =  "";
        putUpdateMessageRequestDto.setMessage(new MessageDTO(null, "굴림체", "#111111", contents, null, 'n', null, paperId, paperTitle));

        //then
        Assertions.assertThatThrownBy(() -> {
            messageController.updateMessage(messageId, putUpdateMessageRequestDto);
        }).isInstanceOf(ConstraintViolationException.class);
    }
}