package MessageToMe.demo.Message;

import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Message.dto.GetMessageListResponseDto;
import MessageToMe.demo.Message.dto.MessageDTO;
import MessageToMe.demo.Paper.Paper;
import MessageToMe.demo.Paper.PaperRepository;
import MessageToMe.demo.Reaction.Reaction;
import MessageToMe.demo.Reaction.ReactionRepository;
import MessageToMe.demo.Sticker.Sticker;
import MessageToMe.demo.Sticker.StickerRepository;
import MessageToMe.demo.User.User;
import MessageToMe.demo.User.UserRepository;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.LongAdderAssert;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Validator;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@SpringBootTest
@Transactional
class MessageServiceTest {

    private User user;
    private Paper paper;
    private Long userId;
    private Long paperId;

    @Autowired
    MessageService messageService;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PaperRepository paperRepository;
    @Autowired
    StickerRepository stickerRepository;
    @Autowired
    ReactionRepository reactionRepository;
    @Autowired
    Validator validatorInjected;

    @BeforeEach
    void 유저_생성(){
        user = new User();
        user.setUserName("jam_test");
        user.setEmail("jam_test@test.com");
        user.setAuthNum(1);
        user.setPassword("jam1234");
        user.setCreateDate(LocalDateTime.now());
        this.user = userRepository.save(user);
        this.userId = this.user.getUserId();
    }

    @BeforeEach
    void 페이퍼_생성(){
        paper = new Paper();
        paper.setPaperTitle("jam's Test Paper");
        paper.setPaperSkin(0);
        paper.setDelyn('n');
        paper.setCreateDate(LocalDateTime.now());
        paper.setOwnerUser(this.user);
        this.paper = paperRepository.save(paper);
        this.paperId = this.paper.getPaperId();
    }

    @Test
    void 메시지_작성() throws ErrorResponse {
        //given
        Long dummy = 0L;
        String email = "jam_test@test.com";
        String font = "굴림체";
        String content  = "메시지 작성 테스트";
        String color = "#123456";
        String userName  = "jam_test";
        char readny  = 'n';
        LocalDateTime createDate = LocalDateTime.now();
        MessageDTO messageDTO = new MessageDTO(dummy, font, color, content, userName, readny, createDate, paperId, paper.getPaperTitle());

        //when
        Long messageId = messageService.save(userId, paperId, messageDTO);

        //then
        Message message = messageRepository.findMessageByMessageId(messageId)
                        .orElseThrow(() -> new IllegalArgumentException("메시지 작성 테스트 실패"));

        Assertions.assertThat(message.getUser().getEmail()).isEqualTo(email);
        Assertions.assertThat(message.getContent()).isEqualTo(content);
    }

    @Test
    void 메시지_수정() throws ErrorResponse {
        //given
        Long dummy = Long.valueOf(0);
        String email = "jam_test@test.com";
        String font = "굴림체";
        String content  = "메시지 수정 테스트";
        String color = "#123456";
        String userName  = "jam_test";
        char readny  = 'n';
        LocalDateTime createDate = LocalDateTime.now();
        MessageDTO messageDTO = new MessageDTO(dummy, font, color, content, userName, readny, createDate, paperId, paper.getPaperTitle());
        Long messageId = messageService.save(userId, paperId, messageDTO);

        // when
        String updateContent  = "메시지 수정 테스트 수정됨";
        MessageDTO updateMessageDTO = new MessageDTO(messageId, font, color, updateContent, userName, readny, createDate, paperId, paper.getPaperTitle());
        messageService.update(messageId, userId, updateMessageDTO);

        // then
        Message message = messageRepository.findMessageByMessageId(messageId)
                .orElseThrow(() -> new IllegalArgumentException("메시지 수정 테스트 실패"));

        Assertions.assertThat(message.getUser().getEmail()).isEqualTo(email);
        Assertions.assertThat(message.getContent()).isEqualTo(updateContent);
    }

    @Test
    void 메시지_삭제() throws ErrorResponse {

        //given
        Long dummy = Long.valueOf(0);
        String email = "jam_test@test.com";
        String font = "굴림체";
        String content  = "메시지 삭제 테스트";
        String color = "#123456";
        String userName  = "jam_test";
        char readny  = 'n';
        LocalDateTime createDate = LocalDateTime.now();
        MessageDTO messageDTO = new MessageDTO(dummy, font, color, content, userName, readny, createDate, paperId, paper.getPaperTitle());
        Long messageId = messageService.save(userId, paperId, messageDTO);

        //when
        messageService.delete(messageId, userId);

        //then
        Message message = messageRepository.findMessageByMessageId(messageId)
                .orElseThrow(() -> new IllegalArgumentException("메시지 삭제 테스트 실패"));

        Assertions.assertThat(message.getUser().getEmail()).isEqualTo(email);
        Assertions.assertThat(message.getDelyn()).isEqualTo('y');
    }

    @Test
    void 모든_메시지_가져오기() throws ErrorResponse {
        //given
        Long dummy = Long.valueOf(0);
        String email = "jam_test@test.com";
        String font = "굴림체";
        String content  = "메시지 삭제 테스트";
        String color = "#123456";
        String userName  = "jam_test";
        char readny  = 'n';
        LocalDateTime createDate = LocalDateTime.now();
        MessageDTO messageDTO = new MessageDTO(dummy, font, color, content, userName, readny, createDate, paperId, paper.getPaperTitle());
        Long messageId = messageService.save(userId, paperId, messageDTO);

        //when
        GetMessageListResponseDto getMessageListResponseDto= messageService.retrieveAll(userId);

        //then
        List<Message> messageList = messageRepository.findAllByUserAndDelynOrderByCreatedateDesc(this.user, 'n')
                .orElseThrow(() -> new IllegalArgumentException("모든 메시지 가져오기 테스트 실패"));

        Assertions.assertThat(getMessageListResponseDto.getMessages().size()).isEqualTo(messageList.size());
    }
}