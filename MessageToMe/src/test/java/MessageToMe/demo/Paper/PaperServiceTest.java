package MessageToMe.demo.Paper;

import MessageToMe.demo.Exception.ErrorResponse;
import MessageToMe.demo.Message.Message;
import MessageToMe.demo.Message.MessageRepository;
import MessageToMe.demo.Paper.dto.*;
import MessageToMe.demo.Reaction.Reaction;
import MessageToMe.demo.Reaction.ReactionRepository;
import MessageToMe.demo.Sticker.Sticker;
import MessageToMe.demo.Sticker.StickerRepository;
import MessageToMe.demo.User.User;
import MessageToMe.demo.User.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@Transactional
public class PaperServiceTest {

    private User user;

    @Autowired
    PaperService paperService;
    @Autowired
    PaperRepository paperRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    MessageRepository messageRepository;
    @Autowired
    ReactionRepository reactionRepository;
    @Autowired
    StickerRepository stickerRepository;

    @BeforeEach
    void 유저_생성() {
        //given
        user = new User();
        user.setUserName("lanto_test");
        user.setEmail("lanto_test@test.com");
        user.setAuthNum(1);
        user.setPassword("lanto1234");
        user.setCreateDate(LocalDateTime.now());
        this.user = userRepository.save(user);
    }

    @Test
    public void 페이퍼_생성_테스트() throws ErrorResponse {
        //given
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);

        //when
        long userId = user.getUserId();
        Paper savePaper = paperService.savePaper(userId, paperDTO);

        //then
        Paper findPaper = paperRepository.findPaperByPaperId(savePaper.getPaperId()).orElseThrow(() -> new IllegalArgumentException("존재하지 않는 페이퍼"));
        Assertions.assertEquals(findPaper.getCreateUser(), user);
        Assertions.assertEquals(findPaper.getPaperTitle(), title);
        Assertions.assertEquals(findPaper.getPaperSkin(), skin);
    }

    @Test
    public void 타이틀_길이_제한_테스트 () {
        //given
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        long userId = user.getUserId();

        //when
        String title = "12345678901";
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> paperService.savePaper(userId, paperDTO));
    }

    @Test
    public void 타이틀_없음_테스트 () {
        //given
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        long userId = user.getUserId();

        //when
        String title = "";
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> paperService.savePaper(userId, paperDTO));
    }

    @Test
    public void 타이틀_중복_테스트 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.of(2022, 1, 2, 12, 34,56,7890);
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        paperService.savePaper(userId, paperDTO);

        //when
        String title2 = "title ~~!!";
        int skin2 = 2;
        LocalDateTime createDate2 = LocalDateTime.now();
        PaperDTO paperDTO2 = new PaperDTO(title2, skin2, createDate2);

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> paperService.savePaper(userId, paperDTO2));
    }

    @Test
    public void 페이퍼_생성은_하루한번_테스트 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        paperService.savePaper(userId, paperDTO);

        //when
        String title2 = "title2222 ~~!!";
        int skin2 = 1;
        LocalDateTime createDate2 = LocalDateTime.now();
        PaperDTO paperDTO2 = new PaperDTO(title2, skin2, createDate2);

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> paperService.savePaper(userId, paperDTO2));
    }

    @Test
    public void 페이퍼_목록_가져오기 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.of(2022, 1, 2, 12, 34,56,7890);
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        paperService.savePaper(userId, paperDTO);

        //when
        String title2 = "title2222 ~~!!";
        int skin2 = 1;
        LocalDateTime createDate2 = LocalDateTime.now();
        PaperDTO paperDTO2 = new PaperDTO(title2, skin2, createDate2);
        paperService.savePaper(userId, paperDTO2);

        //then
        List<PaperListDTO> paperList = paperService.getPaperList(userId, PageRequest.of(0, 2));
        Assertions.assertEquals(2, paperList.size());
    }

    @Test
    public void 선물받은_페이퍼_목록_가져오기 () throws ErrorResponse {
        //given
        long userId = user.getUserId();

        User guestUser = new User();
        guestUser.setEmail("lantoGuest@gmail.com");
        guestUser.setUserName("lantoGuest");
        guestUser.setAuthNum(1);
        guestUser.setPassword("lantoGuest1234");
        guestUser.setCreateDate(LocalDateTime.now());
        userRepository.save(guestUser);

        //when
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.of(2022, 1, 2, 12, 34,56,7890);
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper paper = paperService.savePaper(guestUser.getUserId(), paperDTO);
        paperService.sendPaper(guestUser.getUserId(), user.getEmail(), paper.getPaperId());

        //then
        List<PaperListDTO> paperList = paperService.getPaperList(userId, PageRequest.of(0, 2));
        Assertions.assertEquals(1, paperList.size());
        Assertions.assertEquals(paper.getOwnerUser().getUserId(), user.getUserId());
        Assertions.assertEquals('y', paperList.get(0).getGiftyn());
    }

    @Test
    public void 페이퍼_수정_테스트 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper savePaper = paperService.savePaper(userId, paperDTO);

        //when
        String updateTitle = "update title ~~!!";
        int updateSkin = 3;
        paperDTO.setPaperTitle(updateTitle);
        paperDTO.setSkin(updateSkin);
        paperDTO.setPaperId(savePaper.getPaperId());
        paperService.updatePaper(userId, paperDTO);

        //then
        Paper findPaper = paperRepository.findPaperByPaperId(savePaper.getPaperId()).orElseThrow(() -> new IllegalArgumentException("존재하지 않는 페이퍼"));
        Assertions.assertEquals(findPaper.getOwnerUser(), user);
        Assertions.assertEquals(findPaper.getPaperTitle(), updateTitle);
        Assertions.assertEquals(findPaper.getPaperSkin(), updateSkin);
    }

    @Test
    public void 메시지_수_테스트 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.of(2022, 1, 2, 12, 34,56,7890);
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        long paperId1 = paperService.savePaper(userId, paperDTO).getPaperId();
        paperDTO.setPaperId(paperId1);

        String title2 = "title2222 ~~!!";
        int skin2 = 1;
        LocalDateTime createDate2 = LocalDateTime.of(2022, 1, 4, 12, 34,56,7890);
        PaperDTO paperDTO2 = new PaperDTO(title2, skin2, createDate2);
        Paper paper = paperService.savePaper(userId, paperDTO2);
        paperDTO2.setPaperId(paper.getPaperId());

        List<PaperDTO> paperDTOList = new ArrayList<>();
        paperDTOList.add(paperDTO);
        paperDTOList.add(paperDTO2);

        Message message1 = Message.builder()
                .content("메시지 작성 테스트").font("굴림체").color("#123456").delyn('n')
                .readyn('n').user(user).paper(paper)
                .build();

        Message message2 = Message.builder()
                .content("메시지 작성 테스트2").font("돋움체").color("#00FF00").delyn('n')
                .readyn('n').user(user).paper(paper)
                .build();

        //when
        messageRepository.save(message1);
        messageRepository.save(message2);

        //then
        List<MessageDataDTO> messageData = paperService.findMessageData(paper);
        Assertions.assertEquals(2, messageData.size());
    }

    @Test
    public void 페이퍼_삭제_테스트 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper savePaper = paperService.savePaper(userId, paperDTO);

        //when
        paperService.deletePaper(userId, savePaper.getPaperId());

        //then
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        Assertions.assertEquals('y', savePaper.getDelyn());
        Assertions.assertEquals(date, savePaper.getDeleteDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        Assertions.assertEquals("삭제된 종이입니다.", savePaper.getPaperTitle());
    }

    @Test
    public void 페이퍼_상세보기_테스트 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper savePaper = paperService.savePaper(userId, paperDTO);

        //when
        ResponsePaperDetailListDTO responsePaperDetailListDTO = paperService.getPaperDetail(userId, savePaper.getPaperId());

        //then
        Assertions.assertEquals(title, responsePaperDetailListDTO.getPapers().getPaperTitle());
        Assertions.assertEquals(skin, responsePaperDetailListDTO.getPapers().getSkin());
    }

    @Test
    public void 메시지_목록_가져오기 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper paper = paperService.savePaper(userId, paperDTO);

        Message message1 = Message.builder()
                .content("메시지 작성 테스트").font("굴림체").color("#123456").delyn('n')
                .readyn('n').user(user).paper(paper)
                .build();

        Message message2 = Message.builder()
                .content("메시지 작성 테스트2").font("돋움체").color("#00FF00").delyn('n')
                .readyn('n').user(user).paper(paper)
                .build();

        messageRepository.save(message1);
        messageRepository.save(message2);

        //when
        List<MessageDTO> messageList = paperService.getMessageList(paper);

        //then
        Assertions.assertEquals(2, messageList.size());
    }

    @Test
    public void 메시지_읽음_확인 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.of(2022, 1, 2, 12, 34,56,7890);
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper paper1 = paperService.savePaper(userId, paperDTO);

        String title2 = "title2222 ~~!!";
        int skin2 = 1;
        LocalDateTime createDate2 = LocalDateTime.now();
        PaperDTO paperDTO2 = new PaperDTO(title2, skin2, createDate2);
        Paper paper2 = paperService.savePaper(userId, paperDTO2);

        Message message1 = Message.builder()
                .content("메시지 작성 테스트").font("굴림체").color("#123456").delyn('n')
                .readyn('n').user(user).paper(paper1)
                .build();

        Message message2 = Message.builder()
                .content("메시지 작성 테스트2").font("돋움체").color("#00FF00").delyn('n')
                .readyn('n').user(user).paper(paper2)
                .build();

        long saveMessageId1 = messageRepository.save(message1).getMessageId();
        long saveMessageId2 = messageRepository.save(message2).getMessageId();

        //when
        paperService.updateMessageReadyn(user, paper1);

        //then
        Assertions.assertEquals('y', messageRepository.findMessageByMessageId(saveMessageId1).get().getReadyn());
        Assertions.assertEquals('n', messageRepository.findMessageByMessageId(saveMessageId2).get().getReadyn());
    }

    @Test
    public void 리액션_목록_가져오기 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper paper = paperService.savePaper(userId, paperDTO);

        Message message1 = Message.builder()
                .content("메시지 작성 테스트").font("굴림체").color("#123456").delyn('n')
                .readyn('n').user(user).paper(paper)
                .build();
        Message saveMessage1 = messageRepository.save(message1);

        Reaction reaction1 = new Reaction();
        reaction1.setUser(user);
        reaction1.setMessage(saveMessage1);
        reaction1.setEmoji("👀");
        Reaction saveReaction1 = reactionRepository.save(reaction1);

        Reaction reaction2 = new Reaction();
        reaction2.setUser(user);
        reaction2.setMessage(saveMessage1);
        reaction2.setEmoji("❤");
        Reaction saveReaction2 = reactionRepository.save(reaction2);

        //when
        List<ReactionDTO> reactionList = paperService.getReactionList(paper);

        //then
        Assertions.assertEquals(2, reactionList.size());
        Assertions.assertEquals(saveReaction1.getReactionId(), reactionList.get(0).getReactionId());
        Assertions.assertEquals(saveReaction2.getReactionId(), reactionList.get(1).getReactionId());
    }

    @Test
    public void 스티커_목록_가져오기 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper paper = paperService.savePaper(userId, paperDTO);

        Sticker sticker = new Sticker();
        sticker.setPositionX("200");
        sticker.setPositionY("400");
        sticker.setPaper(paper);
        sticker.setUser(user);
        sticker.setKind(3L);
        Sticker saveSticker = stickerRepository.save(sticker);

        //when
        List<StickerDTO> stickerList = paperService.getStickerList(paper);

        //then
        Assertions.assertEquals(1, stickerList.size());
        Assertions.assertEquals(saveSticker.getStickerId(), stickerList.get(0).getStickerId());
    }

    @Test
    void 선물하기_테스트 () throws ErrorResponse {
        //given
        User guestUser = new User();
        guestUser.setEmail("lantoGuest@gmail.com");
        guestUser.setUserName("lantoGuest");
        guestUser.setAuthNum(1);
        guestUser.setPassword("lantoGuest1234");
        guestUser.setCreateDate(LocalDateTime.now());
        userRepository.save(guestUser);

        long userId = user.getUserId();
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper paper = paperService.savePaper(userId, paperDTO);

        //when
        paperService.sendPaper(userId, guestUser.getEmail(), paper.getPaperId());

        //then
        Assertions.assertEquals(guestUser.getUserId(), paper.getOwnerUser().getUserId());
    }

    @Test
    void 페이퍼_수정_타이틀_길이초과_테스트 () throws ErrorResponse {
        //given
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        paperService.savePaper(user.getUserId(), paperDTO);

        //when
        String updateTitle = "123456789012345678901234567890123456789012345678901";
        int updateSkin = 3;
        paperDTO.setPaperTitle(updateTitle);
        paperDTO.setSkin(updateSkin);

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> paperService.updatePaper(user.getUserId(), paperDTO));
    }

    @Test
    void 페이퍼_수정_타이틀_최소길이_테스트 () throws ErrorResponse {
        //given
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        paperService.savePaper(user.getUserId(), paperDTO);

        //when
        String updateTitle = "";
        int updateSkin = 3;
        paperDTO.setPaperTitle(updateTitle);
        paperDTO.setSkin(updateSkin);

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> paperService.updatePaper(user.getUserId(), paperDTO));
    }

    @Test
    public void 페이퍼_수정_타이틀_중복_테스트 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.of(2022, 1, 2, 12, 34,56,7890);
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        paperService.savePaper(userId, paperDTO);

        String title2 = "title2222 ~~!!!!!!";
        int skin2 = 2;
        LocalDateTime createDate2 = LocalDateTime.now();
        PaperDTO paperDTO2 = new PaperDTO(title2, skin2, createDate2);
        paperService.savePaper(userId, paperDTO2);

        //when
        String updateTitle = "title ~~!!";
        int updateSkin = 3;
        paperDTO2.setPaperTitle(updateTitle);
        paperDTO2.setSkin(updateSkin);

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> paperService.updatePaper(userId, paperDTO2));
    }

    @Test
    public void 페이퍼_수정시_삭제된_페이퍼는_수정하지_못한다 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.of(2022, 1, 2, 12, 34,56,7890);
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper savePaper = paperService.savePaper(userId, paperDTO);
        paperService.deletePaper(userId, savePaper.getPaperId());

        //when
        String updateTitle = "title update update ~~!!";
        int updateSkin = 3;
        paperDTO.setPaperTitle(updateTitle);
        paperDTO.setSkin(updateSkin);

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> paperService.updatePaper(userId, paperDTO));
    }

    @Test
    void 권한없는_페이퍼_삭제는_실패한다 () throws ErrorResponse {
        //given
        User guestUser = new User();
        guestUser.setEmail("lantoGuest@gmail.com");
        guestUser.setUserName("lantoGuest");
        guestUser.setAuthNum(1);
        guestUser.setPassword("lantoGuest1234");
        guestUser.setCreateDate(LocalDateTime.now());
        userRepository.save(guestUser);

        long userId = user.getUserId();
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper savePaper = paperService.savePaper(userId, paperDTO);

        //when
        paperService.sendPaper(userId, guestUser.getEmail(), savePaper.getPaperId());

        //then
        Assertions.assertThrows(ErrorResponse.class, () -> paperService.deletePaper(userId, savePaper.getPaperId()));
    }

    @Test
    public void 선물받은_페이퍼의_메시지는_읽지_않음이어야_한다 () throws ErrorResponse {
        //given
        long userId = user.getUserId();
        String title = "title1111 ~~!!";
        int skin = 2;
        LocalDateTime createDate = LocalDateTime.now();
        PaperDTO paperDTO = new PaperDTO(title, skin, createDate);
        Paper paper = paperService.savePaper(userId, paperDTO);

        User guestUser = new User();
        guestUser.setEmail("lantoGuest@gmail.com");
        guestUser.setUserName("lantoGuest");
        guestUser.setAuthNum(1);
        guestUser.setPassword("lantoGuest1234");
        guestUser.setCreateDate(LocalDateTime.now());
        userRepository.save(guestUser);

        String title2 = "title2222 ~~!!";
        int skin2 = 1;
        LocalDateTime createDate2 = LocalDateTime.now();
        PaperDTO paperDTO2 = new PaperDTO(title2, skin2, createDate2);
        Paper giftPaper = paperService.savePaper(guestUser.getUserId(), paperDTO2);

        Message message1 = Message.builder()
                .content("메시지 작성 테스트").font("굴림체").color("#123456").delyn('n')
                .readyn('n').user(user).paper(paper)
                .build();

        // 페이퍼를 선물 할 때 readyn값이 바뀌어야한다. ('y' -> 'n')
        Message message2 = Message.builder()
                .content("메시지 작성 테스트2").font("돋움체").color("#00FF00").delyn('n')
                .readyn('y').user(user).paper(giftPaper)
                .build();

        messageRepository.save(message1);
        long messageId = messageRepository.save(message2).getMessageId();

        //when
        paperService.sendPaper(guestUser.getUserId(), user.getEmail(), giftPaper.getPaperId());

        //then
        Assertions.assertEquals('n', messageRepository.findMessageByMessageId(messageId).get().getReadyn());
    }

}
