FROM adoptopenjdk/openjdk11
ARG JAR_FILE=MessageToMe/target/demo-0.0.1-SNAPSHOT.jar
EXPOSE 8000
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]